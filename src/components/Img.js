import Component from "./Component.js";

class Img extends Component {
	constructor(value) {
		super('img', { name: 'src', value: value });
	}

	render() {
		return super.render();
	}
}

export default Img;